/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Francisco
 */
public class Basedatoscompras {
     public void InsertarArchivo(String datos){
        try {
            File archivo = new File("Compras_Boletos.txt");
            BufferedWriter BUFF = new BufferedWriter(new FileWriter(archivo, true));
            BUFF.write(datos + "\r\n");
            BUFF.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
    
   public ArrayList<String> LeerArchivo() {
        ArrayList<String> contenido = new ArrayList<>();
        try {
            File archivo = new File("Compras_Boletos.txt");
            BufferedReader BUFF = new BufferedReader(new FileReader(archivo));
            while (BUFF.ready()) {
                contenido.add(BUFF.readLine());
            }
            BUFF.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return contenido;
    }
}
