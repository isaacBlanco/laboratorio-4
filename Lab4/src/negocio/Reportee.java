/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import Datos.Basedatoscompras;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Francisco
 */
public class Reportee {
    private final Basedatoscompras as = new Basedatoscompras();

    public void agregarreport(String Report) {
        as.InsertarArchivo(Report);
    }

    public ArrayList<String> Reporte() {
        return as.LeerArchivo();
    }

    public DefaultTableModel mode(DefaultTableModel mod) {
        ArrayList<String> datos = Reporte();
        if (!datos.isEmpty()) {
            mod.removeRow(0);
            for (String linea : datos) {
                String[] columna = linea.split(",");
                mod.addRow(columna);
            }
        }
        return mod;
    }
}
