/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import Datos.Basedatoscompras;
import java.awt.Color;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Francisco
 */
public class Boletos {
     private String campo, precio;
    private int opSelec = 5;
    private final int fila = 8, columna = 9;
    public int[][] asiento = new int[fila][columna];
    private final String[] Columnas = {"A", "B", "C", "D", "E", "F", "G", "H", "I"};

    private final Basedatoscompras based = new Basedatoscompras();
    private Map<String, Integer> camposs = new HashMap();
    private ArrayList<String> arrayReservated = new ArrayList<>();
    public static ArrayList<String> arrayRandom = new ArrayList<>();

    private void getobtener() {
        ArrayList<String> datos = getDatos();
        for (String linea : datos) {
            String[] parte = linea.split(",");
            String Asientos = parte[3];
            String[] seleccionarAsiento = Asientos.split("-");
            arrayReservated.addAll(Arrays.asList(seleccionarAsiento));
        }
    }

    public void Asientoreservado() {
        getobtener();
        for (int i = 0; i < asiento.length; i++) {
            for (int j = 0; j < asiento[i].length; j++) {
                String contador = String.valueOf(i + 1) + Columnas[j];
                if (arrayReservated.contains(contador)
                        || arrayRandom.contains(contador)) {
                    asiento[i][j] = 1;
                } else {
                   asiento[i][j] = 0;
                }
            }
        }

    }

    private int getRandom(int num) {
        return (int) Math.floor(Math.random() * num);
    }

    public ArrayList<String> Reservadorandom() {
        ArrayList<String> lista = new ArrayList<>();
        int asientoReser = 25;
        while (asientoReser > 0) {
            int ranFila = getRandom(fila), ranColumn = getRandom(columna);
            if (asiento[ranFila][ranColumn] == 0) {
                lista.add(String.valueOf(ranFila+1)+Columnas[ranColumn]);
                asientoReser--;
            }
        }
        return lista;
    }

    public boolean Reservado(int asifila, int asicolum) {
        return asiento[asifila - 1][asicolum] == 0;
    }

    public boolean AsientoComprado(String ced) {
        boolean AF = false;
        ArrayList<String> datos = getDatos();
        for (String linea : datos) {
            String[] parte = linea.split(",");
            String cedula = parte[0];
            if (cedula.equals(ced)) {
                AF = true;
            }
        }
        return AF;
    }

    public void GuardarDatos(String infoPer) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy-hh:mm a");
        String dateTime = dtf.format(LocalDateTime.now());
        String incomp = infoPer + "," + campo + "," + precio + "," + dateTime;
        based.InsertarArchivo(incomp);
    }

    public ArrayList<String> getDatos() {
        return based.LeerArchivo();
    }

    private void getSelecionarAsiento() {
        int resultado = 0;
        String campoasiento = "";
        for (Map.Entry<String, Integer> entry : camposs.entrySet()) {
            String key = entry.getKey();
            int val = entry.getValue();
            campoasiento += key + "-";
            resultado += val;
        }
        if (!campoasiento.isEmpty()) {
            campoasiento = campoasiento.substring(0, campoasiento.length() - 1);
        }
        campo = campoasiento;
        precio = String.valueOf(resultado);
    }

    public Color[] getFondo(String text, Color color) {
        Color[] background = new Color[]{Color.WHITE, Color.BLACK};
        int selectfila = Integer.valueOf(String.valueOf(text.charAt(0))), costo;
        if (color == Color.YELLOW) {
            camposs.remove(text);
            opSelec++;
        } else if (opSelec > 0) {
            background[0] = Color.YELLOW;
            background[1] = Color.WHITE;
            if (selectfila <= 4) {
                costo = 5000;
            } else {
                costo = 3000;
            }
            camposs.put(text, costo);
            opSelec--;
        }
        getSelecionarAsiento();
        return background;
    }

    public String getCampo() {
        return campo;
    }

    public String getPrecio() {
        return precio;
    }

    public int getFila() {
        return fila;
    }

    public int getColumna() {
        return columna;
    }
}
