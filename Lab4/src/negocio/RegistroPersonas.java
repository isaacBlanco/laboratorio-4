/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import Datos.BaseDatosPersonas;
import java.util.ArrayList;

/**
 *
 * @author Francisco
 */
public class RegistroPersonas {
    
    private final BaseDatosPersonas aa = new BaseDatosPersonas();
    private String obten = null;

     public ArrayList<String> getDatos() {
        return aa.leerarchivo();
    }
    public void GuardarDatos(String cedula, String nombre, String genero) {
        String format = cedula + "," + nombre + "," + genero;
        aa.InsertarArchivo(format);
    }

   
    public boolean validarcedula(String cedulaa) {
        boolean enc = false;
        ArrayList<String> datos = getDatos();
        for (String linea : datos) {
            String[] Par = linea.split(",");
            String cedula = Par[0];
            if (cedula.equals(cedulaa)) {
                obten = linea;
                enc = true;
            }
        }
        return enc;
    }
    public String obtener(){
        return obten;
    }

    public boolean validardatos(String cedula, String nombre, String genero) {
        boolean VAL = false;
        if (!cedula.contains("_") && !nombre.isEmpty() && !genero.isEmpty()) {
            VAL = true;
        }
        return VAL;
    }

    
}
